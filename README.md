# MVT - Generátor stromů v THREE.js

## Popis

Umožňuje rekurzivně generovat strom podle uživatelem zadaných parametrů a s využitím THREE.js ho zobrazuje v prohlížeči
ve 3D.

## Screenshoty

![Screenshot](img/readme/screenshot.png) \
![Bříza](img/readme/briza.png) ![Topol](img/readme/topol.png) ![Jehličnan](img/readme/jehlicnan.png)

## Spuštění

Aby bylo možné načíst obrázkové textury, je potřeba spustit lokální server. Pokud je k dispozici python, jde to
jednoduše například:

`python3 -m http.server`

**Ovšem není to nutné**, samotný kód je zabalený tak, že všechna funkcionalita kromě textur jde využít i pouze načtením
souboru index.html.
Potom se hodí v každém řádu změnit materiál na některý bez obrázkové textury

![Změna řádu](img/readme/zmena_radu.png) ![Změna materiálu](img/readme/zmena_materialu.png)

## Použití

Jakmile je aplikace spuštěná, stačí jen měnit hodnoty v menu vlevo, a strom se sám obnovuje.

Úplně nahoře v menu je několik přihrádek obsahující již vytvořené stromy, které je možné načítat a upravovat.\
Úpravy se ukládají do přihrádky a zůstanou až do dalšího obnovení stránky.\
![Změna řádu](img/readme/8.png)

Tlačítkem "Vygenerovat znovu" se vytvoří nový strom se stejnými parametry, který může vypadat jinak, pokud jsou některé
parametry náhodné\
![Změna řádu](img/readme/0.png)

Zde si uživatel vybírá řád větví, který chce měnit. Například kmen je řádu 1. Větve, které z něj vycházejí jsou řádu 2.
Větve, které vycházejí z těchto větví, jsou řádu 3, a tak dál.\
Tlačítkem "+" lze přidat další řád větví, který bude kopií nejvyššího řádu. Tlačítkem "-" se odebere nejvyšší řád
větví.\
![Změna řádu](img/readme/1.png)

Délka větve přímočaře určuje rozsah, jaké délky mohou být větve generované v daném řádu\
![Změna řádu](img/readme/2.png)

Zakřivení větve určuje o kolik stupňů se každý segment (jehož délka je určena dále uvedenou hodnotou "Vzdálenost mezi
pupeny") může odchýlit od toho předchozího (pod ním).\
Rotace určuje otočení kolem své nejdelší osy (zakřivení je chaotičtější), vychýlení pak míru a směr zakřivení.\
![Změna řádu](img/readme/3.png)

V této sekci lze nastavit, jak budou z dané větve (např. kmenu) vyrůstat větve dalšího řádu\
Rotace určuje, jak se střídá směr, kterým větev vyrůstá. Např. rotace 0 znamená, že větve budou růst přímo nad sebou\
Vychýlení určuje jak moc si ponechají směr větve, ze které rostou. Např. pokud je nastavený na 90, větve porostou
kolmo.\
![Změna řádu](img/readme/4.png)

#### Ostatní parametry:

**Vzdálenost mezi pupeny** - větev je složená ze segmentů, mezi kterými probíhá zakřivení, a na jejichž předělech můžou
vyrůstat další větve. Takže čím menší vzdálenost, tím jemnější bude zakřivení a vedlejších větví může vyrůstat více.\
**Procento úspěšnosti pupenů** - pokud je nastavené na 1, na každém předělu mezi segmenty bude alespoň jedna větev. Při
nižším nastavení začnou větve ubývat.\
**Nejvíc větví z jednoho pupenu** - Mezi předěly může růst i víc než jedna větev, zde se určuje jejich počet\
**Objem hlavní větve při dělení** - Hlavní větev si vždy zachová část své velikosti, a zbytek dostane větev, která nově
vzniká.\
Volně založené na: https://www.insidescience.org/news/uncovering-da-vincis-rule-trees \
"All the branches of a tree at every stage of its height when put together are equal in thickness to the trunk. In other
words, if a tree’s branches were folded upward and squeezed together, the tree would look like one big trunk with the
same thickness from top to bottom."\
![Změna řádu](img/readme/5.png)

Zde je možné vybrat jeden z materiálů\
![Změna řádu](img/readme/6.png)

Uživatelem vytvořený strom se ukládá do přihrádky, ve které byl původně předdefinovaný strom. Tyto úpravy se obnovením
stránky nebo kliknutím na tlačítko "Resetovat" smažou.\
![Změna řádu](img/readme/7.png)

## Použité algoritmy

Strom je generovaný rekurzivně, přičemž princip (přidávání nových větvý na předěl segmentů a na vrchol větve) je vždy
stejný, ale každá úroveň rekurze může používat jiné hodnoty ovlivňující, jak strom generovat. V kódu existuje pole
s objekty, které toto chování určují

`new TreeLevel(branch_length, branch_curvature_rotation, branch_curvature_angle, branch_angle, branch_rotation, bud_success, side_branch_distance, side_branch_group, split_volume_ratio, material)`
(assets.js)

Hodnoty v tomto objektu mohou být generovány náhodně, protože jako argumenty přijímá "generátory", které jsou definovány v souboru
randomgenerators.js. Každý generátor si ukládá svoje parametry a hodnotu generuje voláním až .generate()

Při samotném vytváření objektů je využíváno toho, že THREE.js objekty si v sobě udržují pozici atd, takže je stačí duplikovat a
přesunout je na vrchol originálního objektu je pak snažší.

Soubor controls.js obsahuje funkce, které spravují hodnoty v menu vlevo a aktualizují podle nich strom.