/**
 * Module that allows the user to modify the tree
 */
import * as GENERATORS from './randomgenerators.js'
import {create_tree} from './buildtree.js'
import {materials} from './assets.js'
import * as THREE from 'three';

// print HTML to a corresponding div
export function print_gaussian_input(id, min_mean, max_mean, default_mean, max_std, step) {
    document.getElementById(id + "_div").innerHTML = `
    <label for="${id}_mean">μ:</label>
        <input id="${id}_mean" type="number" min="${min_mean}" max="${max_mean}" value="${default_mean}" step="${step}">
        <label for="${id}_std">σ:</label>
        0 <input id="${id}_std" type="range" min="0" max="${max_std}" value="${max_std}" step="${step / 10}"> ${max_std}
    `
}

// print HTML to a corresponding div
export function print_uniform_input(id, a_min, a_max, b_min, b_max, step) {
    document.getElementById(id + "_div").innerHTML = `
    <label for="${id}_a">a:</label>
    <input id="${id}_a" type="number" min="${a_min}" value="${Math.round((a_min + a_max) / 2)}" max="${a_max}" step="${step}">
    <label for="${id}_b">b:</label>
    <input id="${id}_b" type="number" min="${b_min}" value="${Math.round((b_min + b_max) / 2)}" max="${b_max}" step="${step}">
    `
}

// get id of the tree slot currently edited by the user
export function get_tree_variable() {
    return document.getElementById("tree_choice").value
}

// get index of the level currently edited
function get_level() {
    return document.getElementById("level").value - 1
}

// shortcut for getting a value from a html input
function get_val(id) {
    return document.getElementById(id).value
}

// get generators based on values in the html form
function get_gaussian_generator(id) {
    return new GENERATORS.NormalGenerator(get_val(id + "_mean"), get_val(id + "_std"), false, get_val(id + "_negative"))
}

function get_constant_generator(id) {
    return new GENERATORS.ConstantGenerator(get_val(id))
}

function get_uniform_generator(id) {
    return new GENERATORS.UniformGenerator(1, get_val(id))
}

function fill_gaussian(id, generator) {
    document.getElementById(id + "_mean").value = generator.mean
    document.getElementById(id + "_std").value = generator.standard_deviation
}

function fill_uniform(id, generator) {
    document.getElementById(id + "_a").value = generator.min_val
    document.getElementById(id + "_b").value = generator.max_val
}

// update values in the form based on the level currently edited
export function update_level(tree_templates) {
    let level = get_level()
    const tree_var = get_tree_variable()

    const tree_levels = tree_templates[tree_var].length
    document.getElementById("level").max = tree_levels;
    if (document.getElementById("level").value > tree_levels) {
        document.getElementById("level").value = tree_levels
        level = get_level()
    }

    fill_uniform("branch_length", tree_templates[tree_var][level].branch_length)
    fill_gaussian("branch_curvature_rotation", tree_templates[tree_var][level].branch_curvature_rotation)
    fill_uniform("branch_curvature_angle", tree_templates[tree_var][level].branch_curvature_angle)
    fill_uniform("branch_angle", tree_templates[tree_var][level].branch_angle)

    document.getElementById("branch_rotation").value = tree_templates[tree_var][level].branch_rotation.value
    document.getElementById("side_branch_distance").value = tree_templates[tree_var][level].side_branch_distance.value
    document.getElementById("bud_success").value = tree_templates[tree_var][level].bud_success.value
    document.getElementById("side_branch_group").value = tree_templates[tree_var][level].side_branch_group.value
    document.getElementById("split_volume_ratio").value = tree_templates[tree_var][level].split_volume_ratio.value;
    document.getElementById("material").value = tree_templates[tree_var][level].material.name;
}

// generate a new tree based on the values in the form
export function apply_form(scene, meshes, tree_templates) {
    const level = get_level()
    const tree_var = get_tree_variable()

    let a = document.getElementById("branch_length_a").value
    let b = document.getElementById("branch_length_b").value
    tree_templates[tree_var][level].branch_length = new GENERATORS.UniformGenerator(Math.min(a, b), Math.max(a, b))

    a = document.getElementById("branch_curvature_angle_a").value
    b = document.getElementById("branch_curvature_angle_b").value
    tree_templates[tree_var][level].branch_curvature_angle = new GENERATORS.UniformGenerator(Math.min(a, b), Math.max(a, b))

    a = document.getElementById("branch_angle_a").value
    b = document.getElementById("branch_angle_b").value
    tree_templates[tree_var][level].branch_angle = new GENERATORS.UniformGenerator(Math.min(a, b), Math.max(a, b))


    let mean = document.getElementById("branch_curvature_rotation_mean").value
    let std = document.getElementById("branch_curvature_rotation_std").value
    tree_templates[tree_var][level].branch_curvature_rotation = new GENERATORS.NormalGenerator(mean, std, false, true)

    tree_templates[tree_var][level].branch_rotation = new GENERATORS.ConstantGenerator(document.getElementById("branch_rotation").value)
    tree_templates[tree_var][level].bud_success = new GENERATORS.ConstantGenerator(document.getElementById("bud_success").value)
    tree_templates[tree_var][level].side_branch_distance = new GENERATORS.ConstantGenerator(document.getElementById("side_branch_distance").value)
    tree_templates[tree_var][level].side_branch_group = new GENERATORS.ConstantGenerator(document.getElementById("side_branch_group").value)
    tree_templates[tree_var][level].split_volume_ratio = new GENERATORS.ConstantGenerator(document.getElementById("split_volume_ratio").value)
    const material_num = document.getElementById("material").value
    tree_templates[tree_var][level].material = materials[material_num]
    for (let j = 0; j < materials.length; j++) {
        if (j == material_num ) {
            document.getElementById("materials_"+j).className = "active_preview"
        } else {
            document.getElementById("materials_"+j).className = "preview"
        }
    }


    for (let i = 0; i < meshes.length; i++) {
        scene.remove(meshes[i])
        meshes[i].removeFromParent()
        meshes[i].geometry.dispose()
    }

    scene.add(create_tree(meshes, tree_templates[tree_var]))

    // let tree_init_string = `export const default_tree = [\n`
    // for (let i = 0; i < tree_templates[tree_var].length; i++) {
    //     let t = tree_templates[tree_var][i]
    //     tree_init_string += `\tnew TreeLevel(new GENERATORS.UniformGenerator(${t.branch_length.min_val}, ${t.branch_length.max_val}),     new GENERATORS.NormalGenerator(${t.branch_curvature_rotation.mean}, ${t.branch_curvature_rotation.standard_deviation}, false, true),  new GENERATORS.UniformGenerator(${t.branch_curvature_angle.min_val}, ${t.branch_curvature_angle.max_val}), new GENERATORS.UniformGenerator(${t.branch_angle.min_val}, ${t.branch_angle.max_val}), new GENERATORS.ConstantGenerator(${t.branch_rotation.value}), new GENERATORS.ConstantGenerator(${t.bud_success.value}), new GENERATORS.ConstantGenerator(${t.side_branch_distance.value}), new GENERATORS.ConstantGenerator(${t.side_branch_group.value}), new GENERATORS.ConstantGenerator(${t.split_volume_ratio.value}), materials[0]),\n`
    // }
    // tree_init_string += ']'
    // console.log(tree_init_string)
}