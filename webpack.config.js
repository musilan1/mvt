const path = require('path');

module.exports = {
  entry: [
  './randomgenerators.js',
  './util.js',
  './assets.js',
  './buildtree.js',
  './controls.js',
  './index.js'
  ],
  output: {
    path: __dirname,
    filename: 'main.js'
  },
  module: {
    rules: [
      {
        test: /\.(png|jpe?g|gif)$/i,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192,
              name: '[name].[ext]',
              outputPath: 'images/'
            }
          }
        ]
      }
    ]
  }
};
