/**
 * Module for generating the tree objects
 */
import * as THREE from 'three';
import {deg_to_rad, get_top_of_cylinder, volume_to_radius} from './util.js'

// number of radial segments of cylinders that make up the branches
const radial_segments = 16;

export function add_branch(parent_branch, volume, parent_volume, length, rotation, split_angle, material) {
    /**
     * adds a new branch based on a given already existing segment and parameters
     * @param parent_branch - existing segment (will inherit mostly its location)
     * @param volume - volume of the cross-section of the new branch at its tip
     * @param parent_volume - volume of the parent branch at area nearest to the new branch (allows for branch size transition)
     * @param length - length of the new branch
     * @param rotation - determines the direction in which the new branch splits away from its parent
     * @param split_angle - determines how far away it splits away (in degrees)
     * @param material - material to be used for the new branch
     */

    // source from previous segment
    let new_branch = parent_branch.clone()

    // swap geometry
    const radius = Math.max(volume_to_radius(volume), 0.0025)
    const parent_radius = Math.max(volume_to_radius(parent_volume), 0.0025)
    const new_geometry = new THREE.CylinderGeometry(radius, parent_radius, length, radial_segments);
    new_branch.geometry = new_geometry

    // set material
    new_branch.material = material

    // translate by length of the previous segment - that puts the center of this branch on top of the previous branch
    new_branch.translateY(length / 2)

    // split the branch away from the direction of the previous segment
    new_branch.rotateY(deg_to_rad(rotation)) // rotation
    new_branch.rotateX(deg_to_rad(split_angle)) // splitting

    // move away from center, so the bottom of this branch is on top of the previous branch
    let top = get_top_of_cylinder(new_branch, length)
    new_branch.position.set(top.x, top.y, top.z)

    // done
    return new_branch;
}

function build_tree(meshes, level, parent_branch, volume, template) {
    /**
     * recursive function to build a tree mesh
     * prints the current main branch and calls functions to build the side-branches
     * @param level - index in template, which this branch is build from
     * @param starting_point - 3D location of the bud of this branch
     * @param direction - direction of this branch
     * @param radius - radius of this branch at the starting point
     * @param template - template for each branch level
     * */

    // create group that will be returned
    let tree = new THREE.Group();

    // quick access to the template
    const level_properties = template[level]

    // create the first segment of the main branch
    let remaining_length = level_properties.branch_length.generate()

    // subtract it from goal length
    const first_length = level_properties.side_branch_distance.generate()
    remaining_length -= first_length

    // cylinder (branch) to append branch segments to
    let main_segment = parent_branch

    // create and append the rest of the segments until branch hits goal length
    let side_branch_curr_rotation = level_properties.branch_rotation.generate() // direction of splitting
    let main_volume = volume // initialize only
    let side_branch_volume = 0 // initialize only
    do {
        // get length of main branch segment and subtract from goal length of the main branch
        let segment_length = level_properties.side_branch_distance.generate()
        remaining_length -= segment_length;

        // figure out volumes
        const side_branch_count = level_properties.side_branch_group.generate();
        const prev_volume = main_volume
        main_volume = Math.pow(level_properties.split_volume_ratio.generate(), side_branch_count) * volume
        side_branch_volume = (volume - main_volume) / side_branch_count
        volume = main_volume

        // main segment
        main_segment = add_branch(main_segment, main_volume, prev_volume, segment_length, level_properties.branch_curvature_rotation.generate(), level_properties.branch_curvature_angle.generate(), level_properties.material);
        meshes.push(main_segment)
        tree.add(main_segment)

        // side branches
        if (level + 1 < template.length) { // only generate if there is a template for side-branches
            for (let i = 0; i < side_branch_count; i++) {
                if (Math.random() < level_properties.bud_success.generate()) {
                    const starting_segment = add_branch(main_segment, side_branch_volume, side_branch_volume, template[level + 1].side_branch_distance.generate(), side_branch_curr_rotation, level_properties.branch_angle.generate(), template[level + 1].material);
                    meshes.push(starting_segment)
                    tree.add(starting_segment)
                    const following_segments = build_tree(meshes, level + 1, starting_segment, side_branch_volume, template)
                    tree.add(following_segments)
                }

                // update rotation
                side_branch_curr_rotation = parseInt(side_branch_curr_rotation)
                side_branch_curr_rotation += parseInt(level_properties.branch_rotation.generate())
            }
        }
    } while (remaining_length > 0)

    // put a smaller branch on top of the main branch
    if (level + 1 < template.length) {
        const fork = build_tree(meshes, level + 1, main_segment, side_branch_volume, template)
        tree.add(fork)
    }

    return tree
}

export function create_tree(meshes, template, stump_radius = 0.15) {
    /**
     * Creates a tree from a template and returns a group containing it
     */

    // create a new group
    let tree = new THREE.Group();

    // create the first segment (will remain hidden)
    const stump_length = template[0].side_branch_distance.generate()
    let stump_geometry = new THREE.CylinderGeometry(stump_radius, stump_radius, stump_length, radial_segments);
    let stump = new THREE.Mesh(stump_geometry, template[0].material)
    meshes.push(stump)
    stump.translateY(-1.5 - stump_length / 2)

    // recursively build the rest of the tree
    tree.add(build_tree(meshes, 0, stump, Math.PI * stump_radius * stump_radius, template))

    return tree
}

export function add_environment(scene) {
    const grass_geometry = new THREE.CircleGeometry(1, 64);
    // https://freestocktextures.com/texture/grass-ground-green,785.html
    let grass_texture = new THREE.TextureLoader().load('./img/grass.jpg');
    const grass_material = new THREE.MeshBasicMaterial({map: grass_texture, color: 0x97aa00, side: THREE.BackSide});
    const grass = new THREE.Mesh(grass_geometry, grass_material);
    grass.rotateX(90 / 180 * Math.PI)
    grass.translateZ(1.51)
    scene.add(grass);

    scene.background = new THREE.Color(0xc8d3e4)

    // for (let i = -5; i < 5; i++) {
    //     let pointLight = new THREE.PointLight(0xffffff, 1);
    //     pointLight.position.set(10 * i, 5 * i, i * 10);
    //     scene.add(pointLight);
    // }
    const ambient_light = new THREE.AmbientLight( 0xdee9ff, 3 )
    scene.add( ambient_light );

    const directionalLight = new THREE.DirectionalLight( 0xfff5de, 2 );
    scene.add( directionalLight );

    let pointLight = new THREE.PointLight(0xdff5f3, 2);
    pointLight.position.set(4, 3, 3);
    scene.add(pointLight);

    let pointLight2 = new THREE.PointLight(0xff8800, 0.6);
    pointLight2.position.set(2, 2, -3);
    scene.add(pointLight2);

    const number_of_lights = 8
    for ( let i = 0; i < number_of_lights ; i++ ) {
        // const geometry = new THREE.BoxGeometry( 0.1, 0.1, 0.1 );
        // const material = new THREE.MeshBasicMaterial( {color: 0x00ff00} );
        // const light = new THREE.Mesh( geometry, material );
        const radius = 2
        const one_rotation = 2 * Math.PI / number_of_lights

        let light = new THREE.PointLight(0xffaa44, 0.1);
        light.position.set(radius * Math.cos(one_rotation*i), 1.5, radius * Math.sin(one_rotation*i));
        scene.add(light);

        let light2 = new THREE.PointLight(0xdff5f3, 0.1);
        light2.position.set(radius * Math.cos(one_rotation*i), 0.1, radius * Math.sin(one_rotation*i));
        scene.add(light2);
    }
}