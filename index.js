// imports
import * as THREE from 'three';
import {OrbitControls} from 'three/addons/controls/OrbitControls.js';
import {create_tree, add_environment} from './buildtree.js';
import {materials, tree_templates} from './assets.js'
import {
    update_level,
    print_gaussian_input,
    print_uniform_input,
    apply_form,
    get_tree_variable
} from './controls.js';

// set up renderer
const renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);

// set up scene
let scene = new THREE.Scene();
const meshes = []

// set up camera
const camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 10000);

// handle resizing
window.addEventListener('resize', () => {
    renderer.setSize(window.innerWidth, window.innerHeight);
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
})

// set up orbiting
const controls = new OrbitControls(camera, renderer.domElement);
controls.rotateSpeed = 4;
controls.dynamicDampingFactor = 0.15;

camera.position.set(0, 0, 5);
controls.update();

// create tree
scene.add(create_tree(meshes, tree_templates[0]))

// add lights, ground and background
add_environment(scene)

// set up refreshing
function animate() {
    requestAnimationFrame(animate);
    controls.update()
    renderer.render(scene, camera);
}
animate();

// print controls
print_uniform_input("branch_length", 0.1, 10, 0.1, 10, 0.1)
print_gaussian_input("branch_curvature_rotation", 0, 360, 0, 30, 1)
print_uniform_input("branch_curvature_angle", -90, 90, -90, 90, 1)
print_uniform_input("branch_angle", -90, 90, -90, 90, 1)

// print materials
for (let i = 0; i < materials.length; i++) {
    document.getElementById("materials_div").innerHTML += `<img src="img/materials/${i}.png" id="materials_${i}" class="preview" alt="ukázka materiálu">`;
}

for (let i = 0; i < materials.length; i++) {
    (function(i) {
        document.getElementById("materials_" + i).addEventListener('click', function () {
            for (let j = 0; j < materials.length; j++) {
                if (j === i) {
                    document.getElementById("materials_"+j).className = "active_preview"
                } else {
                    document.getElementById("materials_"+j).className = "preview"
                }
            }
            document.getElementById("material").value = i
            // update_level(tree_templates);
            apply_form(scene, meshes, tree_templates)
        });
    })(i);
}

// print tree choices
for (let i = 0; i < tree_templates.length; i++) {
    document.getElementById("tree_templates_div").innerHTML += `<img src="img/trees/${i}.png" id="tree_templates_${i}" class="preview" alt="strom">`
    if ( ! ((i+1) % 4 )) {
        document.getElementById("tree_templates_div").innerHTML += '<br>'
    }
}

for (let i = 0; i < tree_templates.length; i++) {
    (function(i) {
        document.getElementById("tree_templates_" + i).addEventListener('click', function () {
            for (let j = 0; j < tree_templates.length; j++) {
                if (j === i) {
                    document.getElementById("tree_templates_" + j).className = "active_preview"
                } else {
                    document.getElementById("tree_templates_" + j).className = "preview"
                }
            }

            document.getElementById("tree_choice").value = i
            update_level(tree_templates);
            apply_form(scene, meshes, tree_templates)
        });
    })(i);
}

// set level to one in case it's been cached
document.getElementById("level").value = 1
update_level(tree_templates)


// event listeners
// update tree when user changes its properties
let form = document.querySelector('form');
form.addEventListener('change', function () {
    apply_form(scene, meshes, tree_templates)
});

// regenerate tree from the same parameters
document.getElementById("redo").addEventListener('click', function () {
    apply_form(scene, meshes, tree_templates)
});

// update values in a form when user changes which level is being edited
let level_range = document.getElementById("level")
level_range.addEventListener('change', function () {
    update_level(tree_templates);
});

// add a level
document.getElementById("plus").addEventListener('click', function () {
    const tree_var = get_tree_variable()
    const prev_length = tree_templates[tree_var].length
    const level_copy = {...tree_templates[tree_var][prev_length - 1]}
    tree_templates[tree_var].push(level_copy)
    document.getElementById("level").max += 1
    document.getElementById("level").value = prev_length + 1
    update_level(tree_templates);
    apply_form(scene, meshes, tree_templates)
});

// delete last level
document.getElementById("minus").addEventListener('click', function () {
    const tree_var = get_tree_variable()
    if (tree_templates[tree_var].length <= 1) {
        alert("Kmen nelze smazat.")
        return
    }
    tree_templates[tree_var].pop()
    document.getElementById("level").max -= 1
    document.getElementById("level").value = tree_templates[tree_var].length
    update_level(tree_templates);
    apply_form(scene, meshes, tree_templates)
});


apply_form(scene, meshes, tree_templates)