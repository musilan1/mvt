/**
 * Some pre-made templates and materials
 */
import * as THREE from "three";
import * as GENERATORS from "./randomgenerators.js";
import birch_bark_image from './img/birch_bark.png'
import pine_bark_image from './img/pine_bark.png'
import brown_bark_image from './img/brown_bark.jpg'

// class that holds attributes for one tree level
class TreeLevel {
    constructor(branch_length, branch_curvature_rotation, branch_curvature_angle, branch_angle, branch_rotation, bud_success, side_branch_distance, side_branch_group, split_volume_ratio, material) {
        /**
         * all parameters accept a generator from ./randomgenerators.js
         * @param branch_length - length of a branch before it splits or ends
         * @param branch_curvature - how much the branch changes its direction at bud points (places where a new branch could grow)
         * @param branch_angle - degree between the parent branch and the new branches
         * @param branch_rotation - rotation of the growing points of branches across the previous branch
         * @param bud_success - fraction of buds that will turn into branches
         * @param side_branch_distance - distance at which branches will grow from each other from the main branch
         * @param side_branch_group - how many branches should grow from one budding point (useful for coniferous trees)
         * @param split_volume_ratio - how much volume will the main branch keep at each bud (the rest will be given to the side branch or lost)
         */

        this.branch_length = branch_length
        this.branch_curvature_rotation = branch_curvature_rotation
        this.branch_curvature_angle = branch_curvature_angle
        this.branch_angle = branch_angle
        this.branch_rotation = branch_rotation
        this.bud_success = bud_success
        this.side_branch_distance = side_branch_distance
        this.side_branch_group = side_branch_group
        this.split_volume_ratio = split_volume_ratio
        this.material = material
    }
}

// materials
let grass_texture = new THREE.TextureLoader().load( brown_bark_image );
console.log(brown_bark_image)
console.log(pine_bark_image)
console.log(birch_bark_image)
let pine_bark_texture = new THREE.TextureLoader().load( pine_bark_image );
let birch_bark_texture = new THREE.TextureLoader().load( birch_bark_image );
export const materials = [
    new THREE.MeshStandardMaterial({color: 0x1c1c1b, roughness: 0.6, side: THREE.DoubleSide}),
    new THREE.MeshStandardMaterial({color: 0x080503, roughness: 0.7, side: THREE.DoubleSide}),
    new THREE.MeshStandardMaterial({map: grass_texture, color: 0x3d3933, roughness: 0.9, side: THREE.DoubleSide}),
    new THREE.MeshStandardMaterial({map: pine_bark_texture, color: 0x3d3933, roughness: 0.9, side: THREE.DoubleSide}),
    new THREE.MeshStandardMaterial({map: birch_bark_texture, color: 0x404040, roughness: 0.6, side: THREE.DoubleSide}),
    new THREE.MeshStandardMaterial({color: 0x00203b, roughness: 0.2, metalness: 0.7, side: THREE.DoubleSide}),
    new THREE.MeshStandardMaterial({color: 0x140f0a, roughness: 0.7, side: THREE.DoubleSide}),
]
for ( let i = 0; i < materials.length ; i++) {
    materials[i].name = i
}

// tree examples
const default_tree = [
    new TreeLevel(new GENERATORS.UniformGenerator(3, 3),     new GENERATORS.NormalGenerator(0, 0, false, true),  new GENERATORS.UniformGenerator(0, 0), new GENERATORS.UniformGenerator(60, 60), new GENERATORS.ConstantGenerator(90), new GENERATORS.ConstantGenerator(1), new GENERATORS.ConstantGenerator(0.5), new GENERATORS.ConstantGenerator(1), new GENERATORS.ConstantGenerator(0.6), materials[0]),
    new TreeLevel(new GENERATORS.UniformGenerator(2, 2),     new GENERATORS.NormalGenerator(0, 0, false, true),  new GENERATORS.UniformGenerator(0, 0), new GENERATORS.UniformGenerator(60, 60), new GENERATORS.ConstantGenerator(60), new GENERATORS.ConstantGenerator(1), new GENERATORS.ConstantGenerator(0.5), new GENERATORS.ConstantGenerator(1), new GENERATORS.ConstantGenerator(0.6), materials[0]),
    new TreeLevel(new GENERATORS.UniformGenerator(1, 1),     new GENERATORS.NormalGenerator(0, 0, false, true),  new GENERATORS.UniformGenerator(0, 0), new GENERATORS.UniformGenerator(60, 60), new GENERATORS.ConstantGenerator(60), new GENERATORS.ConstantGenerator(1), new GENERATORS.ConstantGenerator(0.5), new GENERATORS.ConstantGenerator(1), new GENERATORS.ConstantGenerator(0.6), materials[0]),
]
const briza = [
    new TreeLevel(new GENERATORS.UniformGenerator(4, 5),     new GENERATORS.NormalGenerator(0, 30, false, true),  new GENERATORS.UniformGenerator(-2, 2    ), new GENERATORS.UniformGenerator(31, 60), new GENERATORS.ConstantGenerator(230), new GENERATORS.ConstantGenerator(0.9), new GENERATORS.ConstantGenerator(0.5), new GENERATORS.ConstantGenerator(3), new GENERATORS.ConstantGenerator(0.88), materials[4]),
    new TreeLevel(new GENERATORS.UniformGenerator(0.7, 2.1),     new GENERATORS.NormalGenerator(29, 25, false, true), new GENERATORS.UniformGenerator(-25, 15), new GENERATORS.UniformGenerator(30, 39), new GENERATORS.ConstantGenerator(230),new GENERATORS.ConstantGenerator(0.5)   , new GENERATORS.ConstantGenerator(0.3), new GENERATORS.ConstantGenerator(4), new GENERATORS.ConstantGenerator(0.9) , materials[4]),
    new TreeLevel(new GENERATORS.UniformGenerator(0.1, 1), new GENERATORS.NormalGenerator(0, 5, false, true),  new GENERATORS.UniformGenerator(0, 30    ), new GENERATORS.UniformGenerator(30, 30), new GENERATORS.ConstantGenerator(230), new GENERATORS.ConstantGenerator(1)   , new GENERATORS.ConstantGenerator(0.15), new GENERATORS.ConstantGenerator(1), new GENERATORS.ConstantGenerator(0.75), materials[0]),
]
const topol = [
    new TreeLevel(new GENERATORS.UniformGenerator(4.7, 5.3),     new GENERATORS.NormalGenerator(0, 30, false, true),  new GENERATORS.UniformGenerator(-4, 4    ), new GENERATORS.UniformGenerator(30, 70), new GENERATORS.ConstantGenerator(230), new GENERATORS.ConstantGenerator(0.9), new GENERATORS.ConstantGenerator(0.7), new GENERATORS.ConstantGenerator(5), new GENERATORS.ConstantGenerator(0.91), materials[2]),
    new TreeLevel(new GENERATORS.UniformGenerator(1.8, 3.2),     new GENERATORS.NormalGenerator(0, 30, false, true), new GENERATORS.UniformGenerator(-35, 45), new GENERATORS.UniformGenerator(50, 70), new GENERATORS.ConstantGenerator(230),new GENERATORS.ConstantGenerator(0.1)   , new GENERATORS.ConstantGenerator(0.1), new GENERATORS.ConstantGenerator(5), new GENERATORS.ConstantGenerator(0.96) , materials[2]),
    new TreeLevel(new GENERATORS.UniformGenerator(0.1, 0.3), new GENERATORS.NormalGenerator(0, 5, false, true),  new GENERATORS.UniformGenerator(-38, 60    ), new GENERATORS.UniformGenerator(60, 60), new GENERATORS.ConstantGenerator(230), new GENERATORS.ConstantGenerator(1)   , new GENERATORS.ConstantGenerator(0.1), new GENERATORS.ConstantGenerator(5), new GENERATORS.ConstantGenerator(0.9), materials[2]),
]
const jehlicnan = [
    new TreeLevel(new GENERATORS.UniformGenerator(4.5, 5.5),     new GENERATORS.NormalGenerator(0, 0, false, true),  new GENERATORS.UniformGenerator(0, 0), new GENERATORS.UniformGenerator(80, 90), new GENERATORS.ConstantGenerator(230), new GENERATORS.ConstantGenerator(0.89), new GENERATORS.ConstantGenerator(0.5), new GENERATORS.ConstantGenerator(5), new GENERATORS.ConstantGenerator(0.94), materials[3]),
    new TreeLevel(new GENERATORS.UniformGenerator(1, 2.5),     new GENERATORS.NormalGenerator(0, 30, false, true),  new GENERATORS.UniformGenerator(-4, 5), new GENERATORS.UniformGenerator(29, 90), new GENERATORS.ConstantGenerator(5), new GENERATORS.ConstantGenerator(0.36), new GENERATORS.ConstantGenerator(0.1), new GENERATORS.ConstantGenerator(2), new GENERATORS.ConstantGenerator(0.96),   materials[3]),
    new TreeLevel(new GENERATORS.UniformGenerator(0.1, 0.3),     new GENERATORS.NormalGenerator(0, 30, false, true),  new GENERATORS.UniformGenerator(-7, 55), new GENERATORS.UniformGenerator(80, 90), new GENERATORS.ConstantGenerator(14), new GENERATORS.ConstantGenerator(1), new GENERATORS.ConstantGenerator(0.1), new GENERATORS.ConstantGenerator(1), new GENERATORS.ConstantGenerator(0.89),  materials[3]),
]

const vrba = [
    new TreeLevel(new GENERATORS.UniformGenerator(3.5, 4.5),     new GENERATORS.NormalGenerator(50, 30, false, true),  new GENERATORS.UniformGenerator(-6, 6), new GENERATORS.UniformGenerator(75, 110), new GENERATORS.ConstantGenerator(114), new GENERATORS.ConstantGenerator(1), new GENERATORS.ConstantGenerator(0.7), new GENERATORS.ConstantGenerator(3), new GENERATORS.ConstantGenerator(0.79), materials[1]),
    new TreeLevel(new GENERATORS.UniformGenerator(0.7, 1.5),     new GENERATORS.NormalGenerator(90, 0, false, true),  new GENERATORS.UniformGenerator(-33, -15), new GENERATORS.UniformGenerator(60, 60), new GENERATORS.ConstantGenerator(270), new GENERATORS.ConstantGenerator(1), new GENERATORS.ConstantGenerator(0.3), new GENERATORS.ConstantGenerator(1), new GENERATORS.ConstantGenerator(0.6), materials[1]),
    new TreeLevel(new GENERATORS.UniformGenerator(0.8, 2.6),     new GENERATORS.NormalGenerator(0, 0, false, true),  new GENERATORS.UniformGenerator(-10, 22), new GENERATORS.UniformGenerator(60, 60), new GENERATORS.ConstantGenerator(60), new GENERATORS.ConstantGenerator(1), new GENERATORS.ConstantGenerator(0.3), new GENERATORS.ConstantGenerator(1), new GENERATORS.ConstantGenerator(0.6),    materials[1]),
]

const magnolie = [
    new TreeLevel(new GENERATORS.UniformGenerator(0.4, 0.4),     new GENERATORS.NormalGenerator(0, 0, false, true),  new GENERATORS.UniformGenerator(0, 0), new GENERATORS.UniformGenerator(30, 85), new GENERATORS.ConstantGenerator(100), new GENERATORS.ConstantGenerator(1), new GENERATORS.ConstantGenerator(0.4), new GENERATORS.ConstantGenerator(8), new GENERATORS.ConstantGenerator(0.72), materials[2]),
    new TreeLevel(new GENERATORS.UniformGenerator(2, 2),     new GENERATORS.NormalGenerator(0, 0, false, true),  new GENERATORS.UniformGenerator(-24, -10), new GENERATORS.UniformGenerator(14, 46), new GENERATORS.ConstantGenerator(43), new GENERATORS.ConstantGenerator(0.75), new GENERATORS.ConstantGenerator(0.5), new GENERATORS.ConstantGenerator(2), new GENERATORS.ConstantGenerator(0.63), materials[1]),
    new TreeLevel(new GENERATORS.UniformGenerator(1, 1),     new GENERATORS.NormalGenerator(0, 0, false, true),  new GENERATORS.UniformGenerator(-22, 30), new GENERATORS.UniformGenerator(60, 60), new GENERATORS.ConstantGenerator(60), new GENERATORS.ConstantGenerator(1), new GENERATORS.ConstantGenerator(0.4), new GENERATORS.ConstantGenerator(1), new GENERATORS.ConstantGenerator(0.16), materials[1]),
]

const osika = [
    new TreeLevel(new GENERATORS.UniformGenerator(3.7, 4.5),     new GENERATORS.NormalGenerator(0, 30, false, true),  new GENERATORS.UniformGenerator(-1, 1), new GENERATORS.UniformGenerator(32, 50), new GENERATORS.ConstantGenerator(100), new GENERATORS.ConstantGenerator(0.6), new GENERATORS.ConstantGenerator(0.6), new GENERATORS.ConstantGenerator(4), new GENERATORS.ConstantGenerator(0.9), materials[4]),
    new TreeLevel(new GENERATORS.UniformGenerator(1, 2.5),     new GENERATORS.NormalGenerator(0, 11.7, false, true),  new GENERATORS.UniformGenerator(-12, -7), new GENERATORS.UniformGenerator(15, 45), new GENERATORS.ConstantGenerator(230), new GENERATORS.ConstantGenerator(0.51), new GENERATORS.ConstantGenerator(0.3), new GENERATORS.ConstantGenerator(2), new GENERATORS.ConstantGenerator(0.77), materials[0]),
    new TreeLevel(new GENERATORS.UniformGenerator(0.7, 1.5),     new GENERATORS.NormalGenerator(0, 0, false, true),  new GENERATORS.UniformGenerator(-12, -7), new GENERATORS.UniformGenerator(30, 39), new GENERATORS.ConstantGenerator(230), new GENERATORS.ConstantGenerator(0.5), new GENERATORS.ConstantGenerator(0.3), new GENERATORS.ConstantGenerator(4), new GENERATORS.ConstantGenerator(0.9), materials[0]),
]

const nevim = [
    new TreeLevel(new GENERATORS.UniformGenerator(3.1, 3.4),     new GENERATORS.NormalGenerator(20, 0, false, true),  new GENERATORS.UniformGenerator(-5, 10), new GENERATORS.UniformGenerator(10, 80), new GENERATORS.ConstantGenerator(25), new GENERATORS.ConstantGenerator(0.8), new GENERATORS.ConstantGenerator(0.3), new GENERATORS.ConstantGenerator(2), new GENERATORS.ConstantGenerator(0.87),  materials[5]),
    new TreeLevel(new GENERATORS.UniformGenerator(2, 3),     new GENERATORS.NormalGenerator(0, 30, false, true),  new GENERATORS.UniformGenerator(-60, 60), new GENERATORS.UniformGenerator(31, 60), new GENERATORS.ConstantGenerator(230), new GENERATORS.ConstantGenerator(0.26), new GENERATORS.ConstantGenerator(0.2), new GENERATORS.ConstantGenerator(1), new GENERATORS.ConstantGenerator(0.87),   materials[5]),
    new TreeLevel(new GENERATORS.UniformGenerator(0.5, 1.7),     new GENERATORS.NormalGenerator(0, 0, false, true),  new GENERATORS.UniformGenerator(40, 40), new GENERATORS.UniformGenerator(31, 60), new GENERATORS.ConstantGenerator(230), new GENERATORS.ConstantGenerator(0.47), new GENERATORS.ConstantGenerator(0.1), new GENERATORS.ConstantGenerator(1), new GENERATORS.ConstantGenerator(0.88), materials[5]),
]

export let tree_templates = [briza, topol, jehlicnan, vrba, magnolie, osika, default_tree, nevim]