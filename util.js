// degrees to radians
import * as THREE from "three";

export function deg_to_rad(deg){
    return deg / 180 * Math.PI
}

// finds point for appending the next branch segment
export function get_top_of_cylinder(cylinder_mesh, length) {
    let position = cylinder_mesh.position
    let point = new THREE.Vector3( 0, length, 0 );
    point.applyEuler( cylinder_mesh.rotation );

    const x = position.x + point.x / 2
    const y = position.y + point.y / 2
    const z = position.z + point.z / 2

    return new THREE.Vector3( x, y, z );
}

// returns the radius of a circle based on its volume
export function volume_to_radius(volume) {
    return Math.sqrt(volume/Math.PI)
}