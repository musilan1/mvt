// gaussian
export class NormalGenerator {
    constructor(mean = 0, standard_deviation = 1, generate_int = false, allow_negative = false) {
        this.mean = mean
        this.standard_deviation = standard_deviation
        this.generate_int = generate_int
        this.allow_negative = allow_negative
    }

    generate() {
        let result = -1
        do {
            // Box-Muller transform
            const u1 = Math.random();
            const u2 = Math.random();
            const z0 = Math.sqrt(-2.0 * Math.log(u1)) * Math.cos(2.0 * Math.PI * u2);
            result = z0 * this.standard_deviation + this.mean;
        } while ( ! this.allow_negative && result >= 0 )

        if (this.generate_int){
            return Math.round(result)
        } else {
            return result
        }
    }
}

// return a number from a range
export class UniformGenerator {
    constructor(min_val = 0, max_val = 1, generate_int = false) {
        this.min_val = min_val
        this.max_val = max_val
        this.generate_int = generate_int
    }

    generate() {
        if (this.generate_int) {
            return Math.floor(Math.random() * (this.max_val - this.min_val + 1)) + this.max_val;
        } else {
            return Math.random() * (this.max_val - this.min_val) + this.min_val;
        }
    }
}

// doesn't employ randomness, always returns the same number
export class ConstantGenerator {
    constructor(value = 0) {
        this.value = value
    }

    generate() {
        return this.value
    }
}